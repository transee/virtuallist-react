## Introduce

[![NPM](https://img.shields.io/npm/v/virtuallist-antd.svg)](https://www.npmjs.com/package/virtuallist-antd) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

这是一个基于 react 所开发的虚拟列表组件。

## Install

```js
npm install virtuallist-react --save
```

## Gitee address

https://gitee.com/transee/virtuallist-react

## Usage

```js
import React from "react";
import ReactDOM from "react-dom";
import VirtualList from 'virtuallist-react';
const data = new Array(5_000_000).fill(0).map((item) => Math.random());
ReactDOM.render(
  <div>
    <VirtualList
      options={{
        width: "100%",
        height: "100%",
        count: data.length,
        itemWidth: "100%",
        itemHeight: 200,
        delay: 30,
        appendNum: 10,
        loading: (
          <div style={{ background: "red", height: "100%" }}>读取中......</div>
        ),
        renderItem: ({ index, style }) => (
          <div key={data[index]} style={style}>
            <img
              alt={index}
              height={200}
              src={
                "https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png?" +
                Math.random()
              }
            />
            data: {data[index]}, Row: #{index}
          </div>
        ),
      }}
    />
  </div>,
  document.getElementById("root")
);
```

## Props

```js
interface VirtualListProps {
    options: {
        count: number;//数据总数，目前仅支持数组长度
        width: string | number;//整个容器宽度
        height: string | number;//整个容器高度
        itemWidth: string|number;//每一行的宽度，建议100%
        itemHeight: number;//每一行的高度
        loading?: JSX.Element;//最底部追加的提示元素
        appendNum?:number;//每次页面展示多少个是根据每行高度和容器高度来计算的，这里可以追加多一些，这样的话滚动起来看着更加顺畅。
        bothAppend?:number;//上下都追加多少项目，使得上下滚动都更加流畅些，提前请求。
        renderItem: Function;//每一项渲染时的回调函数，在这里定义每行具体的内容展示
        delay:number;//其实是一个节流函数，防止滚动触发过快导致资源反复加载，浪费大量性能。可以根据自己需要控制
        dom?: number;//支持传入一个ref，拿到容器dom，可以对容器位置等进行控制。举个例子：虚拟滚动跟传统的直铺最大的用户体验问题在于没有浏览器在带的crtl+f搜索，那么父级处理完搜索跳转到第几行，这里传入ref，拿到了dom以后，就可以在对应的操作里让容器滚动到指定的地方，实现crtl+f搜索。该特性在0.0.4开始支持。
    }
}
```

