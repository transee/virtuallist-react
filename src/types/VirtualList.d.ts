interface VirtualListProps {
    options: {
        count: number;
        width: string | number;
        height: string | number;
        itemWidth: string|number;
        itemHeight: number;
        loading?: JSX.Element;
        appendNum?:number;
        renderItem: Function;
        delay:number;
        bothAppend?: number;
        // dom?:React.Ref<HTMLDivElement>
        dom?: any;
    }
}
