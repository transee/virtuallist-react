function debounce(fn: () => void, time: number) {
    let timer: NodeJS.Timeout;
    return function () {
      clearInterval(timer);
      timer = setTimeout(() => {
        fn();
      }, time);
    };
  }

export {
    debounce
}